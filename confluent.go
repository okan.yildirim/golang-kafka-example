package main

import (
	"encoding/json"
	"fmt"
	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
	"strconv"
	"time"
)

func produceByConfluent() {

	producer, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": broker})

	if err != nil {
		panic("Exception occurred when creating producer" + err.Error())
	}

	defer producer.Close()

	go func() {
		for e := range producer.Events() {
			switch ev := e.(type) {
			case *kafka.Message:
				if ev.TopicPartition.Error != nil {
					fmt.Printf("Failed to deliver message: %v\n", ev.TopicPartition)
				} else {
					fmt.Printf("Successfully produced record to confluentTopic %s partition [%v] offset [%v]\n",
						*ev.TopicPartition.Topic, ev.TopicPartition.Partition, ev.TopicPartition.Offset)
				}
			}
		}
	}()

	for i := 0; i < 100; i++ {

		key := strconv.Itoa(i)
		user := getRandomUser()

		value, err := json.Marshal(user)
		if err != nil {
			panic("Exception occurred while marshalling random user")
		}

		topic := confluentTopic
		producer.Produce(&kafka.Message{
			TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
			Key:            []byte(key),
			Value:          value,
		}, nil)

		fmt.Println("Message Produced #", i)
		time.Sleep(time.Second)
	}

	producer.Flush(10 * 1000)
}

func consumeByConfluent() {

	consumer, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": broker,
		"group.id":          "my-confluent-consumer",
		"auto.offset.reset": "earliest",
	})

	if err != nil {
		panic("Exception occurred when creating consumer" + err.Error())
	}

	err = consumer.Subscribe(confluentTopic, nil)

	for {
		message, err := consumer.ReadMessage(-1)

		if err != nil {
			panic("Exception occurred when consuming message " + err.Error())
		}

		fmt.Printf("Message consumed. Topic:%s, Partition:%v, Offset:%v, Message: %s\n",
			*message.TopicPartition.Topic, message.TopicPartition.Partition, message.TopicPartition.Offset, string(message.Value))

		var myUser User
		err = json.Unmarshal(message.Value, &myUser)

		fmt.Printf("Consumed User -> Name: %v Age: %v, salary: %v\n", myUser.Name, myUser.Age, myUser.Salary)
	}
	consumer.Close()
}
