package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/segmentio/kafka-go"
	"log"
	"os"
	"strconv"
	"time"
)

func produceBySegmentio(ctx context.Context) {

	logger := log.New(os.Stdout, "Kafka Producer: ", 0)
	writer := kafka.Writer{
		Addr:   kafka.TCP(broker),
		Topic:  segmentioTopic,
		Logger: logger}

	for i := 0; i < 100; i++ {
		key := strconv.Itoa(i)

		user := getRandomUser()
		value, _ := json.Marshal(user)

		err := writer.WriteMessages(ctx, kafka.Message{
			Key:   []byte(key),
			Value: value,
		})

		if err != nil {
			panic("Exception occurred while sending message " + key + "  Error:  " + err.Error())
		}

		fmt.Println("Message Produced #", i)
		time.Sleep(time.Second)
	}
}

func consumeBySegmentio(ctx context.Context) {

	logger := log.New(os.Stdout, "Kafka Reader: ", 0)

	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{broker},
		GroupID: "my-go-consumer",
		Topic:   segmentioTopic,
		Logger:  logger})

	for {
		message, err := reader.ReadMessage(ctx)

		if err != nil {
			panic("Exception occurred while consuming segmentioTopic. Error: " + err.Error())
		}

		fmt.Println("Message consumed. " +
			" Topic: " + message.Topic +
			" Partition: " + strconv.Itoa(message.Partition) +
			" Offset: " + strconv.FormatInt(message.Offset, 10) +
			" Value: " + string(message.Value))

		var myUser User
		err = json.Unmarshal(message.Value, &myUser)

		fmt.Printf("Consumed User -> Name: %v Age: %v, salary: %v\n", myUser.Name, myUser.Age, myUser.Salary)
	}

}
