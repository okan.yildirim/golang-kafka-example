package main

import (
	"context"
	"fmt"
	"github.com/pioz/faker"
)

const (
	broker         = "localhost:9092"
	segmentioTopic = "my-segmentio-topic"
	confluentTopic = "my-confluent-topic"
)

type User struct {
	Name   string  `json:"name"`
	Age    int     `json:"age"`
	Salary float32 `json:"salary"`
}

func main() {
	libraryChoice := "segmentio"
	switch libraryChoice {
	case "segmentio":
		ctx := context.Background()
		go produceBySegmentio(ctx)
		consumeBySegmentio(ctx)
	case "confluent":
		 go produceByConfluent()
		 consumeByConfluent()
	default:
		fmt.Println("Incorrect choice!!")
	}

}

func getRandomUser() User {
	return User{Name: faker.FirstName() + " " + faker.LastName(),
		Age:    faker.IntInRange(3, 99),
		Salary: faker.Float32InRange(1000.00, 999999.99)}
}
