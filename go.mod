module golang-kafka-example

go 1.15

require (
	github.com/confluentinc/confluent-kafka-go v1.7.0 // indirect
	github.com/pioz/faker v1.7.2
	github.com/segmentio/kafka-go v0.4.23
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.7.0
)
